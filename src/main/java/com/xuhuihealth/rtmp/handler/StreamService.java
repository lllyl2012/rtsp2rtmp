package com.xuhuihealth.rtmp.handler;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class StreamService {

    @Async
    public void play(String streamUrl,Integer i) {
        try {
            new ConvertVideoPakcet().from(streamUrl)
                    .to("rtmp://localhost:1935/live/"+i)
                    .go();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
